package com.company;

import java.util.Scanner;

/**
 * @author Yusuf Kristianto
 * @since 2020/06/11
 */

public class SimpleConsoleDrawing {
    public SimpleConsoleDrawing(){

    }

    public void doSimpleConsoleDrawing(){
        String command;
        String[][] canvas = new String[][]{};
        Integer w=0, h=0;
        do {
            System.out.print("enter command:");
            Scanner scanner = new Scanner(System.in);
            command = scanner.nextLine();
            System.out.println();

            if (command.length() > 1) {
                String[] splittedCommand = command.split(" ");
                switch (splittedCommand[0]) {
                    case "C":
                        w = Integer.valueOf(splittedCommand[1]);
                        h = Integer.valueOf(splittedCommand[2]);
                        canvas = new String[h + 2][w + 2];
                        fillCanvas(w, h, canvas);
                        break;
                    case "L": {
                        Integer x1 = Integer.valueOf(splittedCommand[1]);
                        Integer y1 = Integer.valueOf(splittedCommand[2]);

                        Integer x2 = Integer.valueOf(splittedCommand[3]);
                        Integer y2 = Integer.valueOf(splittedCommand[4]);

                        if (y1.compareTo(y2) == 0) {
                            for (int j = x1; j <= x2; j++) {
                                canvas[y1][j] = "x";
                            }
                        } else {
                            for (int j = y1; j <= y2; j++) {
                                canvas[j][x1] = "x";
                            }
                        }
                        break;
                    }
                    case "R": {

                        Integer x1 = Integer.valueOf(splittedCommand[1]);
                        Integer y1 = Integer.valueOf(splittedCommand[2]);

                        Integer x2 = Integer.valueOf(splittedCommand[3]);
                        Integer y2 = Integer.valueOf(splittedCommand[4]);

                        if (y1.compareTo(y2) == 0 || x1.compareTo(x2) == 0) {
                            System.out.println("Thats a line, not a rectangle");
                        } else {
                            for (int i = y1; i < y2 + 1; i++) {
                                for (int j = x1; j < x2 + 1; j++) {
                                    if (i == y1 || i == y2) {
                                        canvas[i][j] = "x";
                                    } else if (j == x1 || j == x2) {
                                        canvas[i][j] = "x";
                                    } else {
                                        canvas[i][j] = " ";
                                    }
                                }
                            }
                        }
                        break;
                    }case "B": {
                        Integer x1 = Integer.valueOf(splittedCommand[1]);
                        Integer y1 = Integer.valueOf(splittedCommand[2]);
                        String color = splittedCommand[3];
                        floodFill(canvas, x1, y1, color);
                        floodFill(canvas, x1, y1 + 1, color);
                        break;
                    }
                }
                drawCanvas(w, h, canvas);
            }

        } while (command.compareTo("Q") != 0);
    }

    private void floodFill(String[][] canvas, int x, int y, String color) {
        if (!canvas[y][x].equalsIgnoreCase(" ")) {
            return;
        }
        canvas[y][x] = color;
        floodFill(canvas, x + 1, y, color);
        floodFill(canvas, x - 1, y, color);
        floodFill(canvas, x, y+1, color);
        floodFill(canvas, x, y - 1, color);
    }

    private void drawCanvas(int w, int h, String[][] canvas) {
        for (int i = 0; i < h + 2; i++) {
            for (int j = 0; j < w + 2; j++) {
                System.out.print(canvas[i][j]);
            }
            System.out.println();
        }
    }

    private void fillCanvas(int w, int h, String[][] canvas) {
        for (int i = 0; i < h + 2; i++) {
            for (int j = 0; j < w + 2; j++) {
                if (i == 0 || i == h + 1) {
                    canvas[i][j] = "-";
                } else if (j == 0 || j == w + 1) {
                    canvas[i][j] = "|";
                } else {
                    canvas[i][j] = " ";
                }

            }
        }
    }
}
