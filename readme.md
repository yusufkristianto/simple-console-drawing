# Simple Console Drawing App

This program made using Java 8.


# How to compile
I've include compiled jar named `SimpleConsoleDrawing.jar`

Open command-line.

`cd path/to/file`

Run the program using `java -jar SimpleConsoleDrawing.jar`
1. First you need to define the canvas by using command with prefix `C`, first number will be the width, second will be the height, the command separated by `<space>`. Example: `C 20 4`. It will produce canvass `20x4`.
2. Type command with prefix `L` to draw a line, and don't forget to define `(x1,y1) and (x2,y2)`. Example `L 5 3 6 3`.
3. Type command with prefix `R` to draw rectangle. First point will be the point on the top left side, and the second point will be the bottom right side. Example `R 14 1 18 3`
4. Type command with prefix `B` to fill the gap, its like `bucket fill` from paint, don't forget to define the point where coloring process started, the last character is for the `color`. Example `B 10 3 o`
5. Type `Q` to exit.